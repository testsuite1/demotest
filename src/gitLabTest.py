#!/usr/bin/env python
###########################################################
# Copyright (c) 2022
#
# Author:       Nektar Volanaki
# Datum:        So 29 Mai 2022 19:34:23 CEST
# Version:      0.01
#
# Description:
#
# Last Update:  So 29 Mai 2022 19:34:23 CEST
# Release:      DEBUG
# Branch:       master
# File:         gitLabTest.py
# Commiter:     Nektar Volanaki
# Comment:
###########################################################
def doNothing():
    """
    Diese Funktion tut wie der Name sagt gar nichts.
    Args: NONE
    Returns: NONE
    """
    pass


def doSomething():
    """
    Diese Funktion tut entgegen ihrem Name gar nichts.
    Args: NONE
    Returns: NONE
    """
    pass


def doSomethingMore():
    """
    Diese Funktion tut entgegen ihrem Name gar nichts.
    Args: NONE
    Returns: NONE
    """
    pass


def doSomethingElse():
    """
    Diese Funktion tut entgegen ihrem Name gar nichts.
    Args: NONE
    Returns: NONE
    """
    pass


def main():
    """
    Diese Funktion ruft nur doNothing auf.
    Args: NONE
    Returns: NONE
    """
    doSomething()
    doNothing()


if __name__ == "__main__":
    main()
    msg = "test"
    print(msg)
