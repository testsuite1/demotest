from src.gitLabTest import doNothing, main


def test_doNothing() -> None:
    assert doNothing() is None


def test_main() -> None:
    assert main() is None
